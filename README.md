# GraphQL API

ApolloServer API used to send the datas to the front end

## Run

1. `git clone git@gitlab.com:abrastats/api.git`
2. `cd api && npm install`
3. In another terminal launch the command `redis-server` if it's not running yet
4. `npm run start-dev`
5. Visit `https://studio.apollographql.com/sandbox/explorer` to query, API is setup at `localhost:4000`

## Contribute

To contribute to the project, fork the project in your own repository, create a new branch called `feature/description` or `fix/description` and open a merge request for it on the main repo.

If you need to do so, don't forget to create the same branch for the scripts and the front respositories.

- Create the `./src/DataSources/DataSource` required for your query (don't forget to instanciate it in `./src/index.js`)
- Create the schema of your query and the response type in `./src/schema.js`
- Add the resolver in `./src/resolvers.js` and use it to send back the DataSource result fetched from redis

Contact Clonescody#1164 on the [Abracadabra Discord](https://discord.gg/pbmftrJ2) for further informations.

## Todo

1. Switch to Typescript
2. Optimize the resolvers

const resolvers = {
  Query: {
    getCauldrons: async (parent, args, { dataSources }) => {
      const cauldrons = await dataSources.cauldrons.fetchCauldrons();
      return cauldrons;
    },
    getCauldron: async (parent, args, { dataSources }) => {
      const cauldrons = await dataSources.cauldrons.fetchCauldrons();
      return cauldrons.find((item) => item.address === args.address);
    },
    getDailyStakingRatio: async (parent, args, { dataSources }) => {
      const stakingRatio = await dataSources.stakingRatio.fetchStakingRatio();
      return stakingRatio;
    },
    getTreasury: async (parent, args, { dataSources }) => {
      const treasury = await dataSources.treasury.fetchTreasury();
      return treasury;
    },
    getPegs: async (parent, args, { dataSources }) => {
      const pegs = await dataSources.pegs.fetchPegs();
      return pegs;
    },
    getTotalTvl: async (parent, args, { dataSources }) => {
      const cauldrons = await dataSources.cauldrons.fetchCauldrons();
      let totalTvl = 0.0;
      cauldrons.forEach((cauldron) => {
        totalTvl += cauldron.tvl;
      });
      return `${totalTvl}`;
    },
    getTotalWeeklyFees: async (parent, args, { dataSources }) => {
      const cauldrons = await dataSources.cauldrons.fetchCauldrons();
      let totalWeeklyFees = 0.0;
      cauldrons.forEach((cauldron) => {
        let cauldronWeeklyFees = 0.0;
        for (
          let i = cauldron.fees.length > 5 ? 4 : cauldron.fees.length - 1;
          i >= 0;
          i--
        ) {
          cauldronWeeklyFees += cauldron.fees[i].fees;
        }
        totalWeeklyFees += cauldronWeeklyFees;
      });
      return `${totalWeeklyFees}`;
    },
    getCumulativeFees: async (parent, args, { dataSources }) => {
      const cauldrons = await dataSources.cauldrons.fetchCauldrons();
      let totalFees = 0.0;
      cauldrons.forEach((cauldron) => {
        cauldron.fees.forEach((dailyFee) => {
          totalFees += dailyFee.fees;
        });
      });
      return `${totalFees}`;
    },
  },
};

export default resolvers;

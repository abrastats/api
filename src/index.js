import { ApolloServer } from "apollo-server";
import Redis from "ioredis";

import typeDefs from "./schema.js";
import resolvers from "./resolvers.js";

import TreasuryDataSource from "./DataSources/TreasuryDataSource.js";
import StakingDataSource from "./DataSources/StakingDataSource.js";
import CauldronsDataSource from "./DataSources/CauldronsDataSource.js";
import FeesDataSource from "./DataSources/FeesDataSource.js";
import PegsDataSource from "./DataSources/PegsDataSource.js";

let redis = new Redis("redis://localhost:6379");

const apolloServer = new ApolloServer({
  typeDefs: typeDefs,
  resolvers: resolvers,
  csrfPrevention: true,
  dataSources: () => ({
    stakingRatio: new StakingDataSource(redis),
    cauldrons: new CauldronsDataSource(redis),
    treasury: new TreasuryDataSource(redis),
    dailyFees: new FeesDataSource(redis),
    pegs: new PegsDataSource(redis),
  }),
});

apolloServer.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});

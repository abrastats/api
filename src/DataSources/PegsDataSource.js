export default class PegsDataSource {
  constructor(redis) {
    this.redis = redis;
  }

  async fetchPegs() {
    const pegs = await this.redis.get("abrastats.pegs");
    return JSON.parse(pegs);
  }
}

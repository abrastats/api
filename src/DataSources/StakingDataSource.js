export default class StakingDataSource {
  constructor(redis) {
    this.redis = redis;
  }

  async fetchStakingRatio() {
    const stakingRatio = await this.redis.get("abrastats.stakingRatio");
    return JSON.parse(stakingRatio);
  }
}

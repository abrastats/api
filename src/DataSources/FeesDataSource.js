export default class FeesDataSource {
  constructor(redis) {
    this.redis = redis;
  }

  async fetchDailyFees() {
    const dailyFees = await this.redis.get("abrastats.dailyFees");
    return JSON.parse(dailyFees);
  }
}

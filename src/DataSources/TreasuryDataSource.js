export default class TreasuryDataSource {
  constructor(redis) {
    this.redis = redis;
  }

  async fetchTreasury() {
    const treasury = await this.redis.get("abrastats.treasury");
    return JSON.parse(treasury);
  }
}

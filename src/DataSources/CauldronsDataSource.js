export default class CauldronsDataSource {
  constructor(redis) {
    this.redis = redis;
  }

  async fetchCauldrons() {
    const cauldrons = await this.redis.get("abrastats.cauldrons");
    return JSON.parse(cauldrons);
  }
}

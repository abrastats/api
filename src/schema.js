import { gql } from "apollo-server";

const typeDefs = gql`
  scalar BigInt
  scalar Timestamp

  type Query {
    getDailyStakingRatio: [DailyStakingRatio!]!
    getCauldron(address: String!): Cauldron
    getCauldrons: [Cauldron!]!
    getTotalTvl: String!
    getTotalWeeklyFees: String!
    getCumulativeFees: String!
    getTreasury: Treasury!
    getPegs: [Peg!]!
  }

  type Peg {
    peg: String!
    reserves: [String!]!
  }

  type Treasury {
    address: String!
    value: ValueObject!
    veCrv: VeCrv!
    assets: [TreasuryAsset!]!
  }

  type ValueObject {
    usd: String!
    eth: String
    btc: String
  }

  type VeCrv {
    value: ValueObject!
    votingPower: String!
    totalLocked: String!
  }

  type TreasuryAsset {
    name: String!
    address: String!
    type: String!
    categories: [String!]
    amount: String!
    price: String
    value: ValueObject!
  }

  type Collateral {
    address: String!
    name: String!
  }

  type Cauldron {
    id: ID!
    address: String!
    name: String!
    bentoBox: String!
    isDeprecated: Boolean!
    network: Int!
    assetType: Int!
    liquidationFee: Float!
    borrowFee: Float!
    interest: Float!
    withdrawableAmount: String
    borrowedMIMs: String!
    availableMIMs: String!
    collateral: Collateral!
    tvl: Float!
    fees: [DailyFees]
  }

  type DailyStakingRatio {
    ratio: Float!
    timestamp: Timestamp!
  }

  type DailyTvl {
    tvl: Float!
    timestamp: Timestamp!
  }

  type DailyFees {
    fees: Float!
    timestamp: Timestamp!
  }
`;

export default typeDefs;
